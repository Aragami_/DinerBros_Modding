namespace MultiplayerWaiters
{
    public static class PluginInfo
    {
        public const string PLUGIN_GUID = "MultiplayerWaiters";
        public const string PLUGIN_NAME = "aragami.dinerbros_mods.MultiplayerWaiters";
        public const string PLUGIN_VERSION = "1.0.0";
    }
}
