//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Reflection;

[assembly: System.Reflection.AssemblyCompanyAttribute("com.aragami.enhanced_endless")]
[assembly: System.Reflection.AssemblyConfigurationAttribute("Debug")]
[assembly: System.Reflection.AssemblyDescriptionAttribute("Enhances Endless Mode")]
[assembly: System.Reflection.AssemblyFileVersionAttribute("0.0.1.0")]
[assembly: System.Reflection.AssemblyInformationalVersionAttribute("0.0.1")]
[assembly: System.Reflection.AssemblyProductAttribute("enhanced_endless")]
[assembly: System.Reflection.AssemblyTitleAttribute("EnhancedEndless")]
[assembly: System.Reflection.AssemblyVersionAttribute("0.0.1.0")]

// Von der MSBuild WriteCodeFragment-Klasse generiert.

