namespace EnhancedEndless
{
    public static class PluginInfo
    {
        public const string PLUGIN_GUID = "EnhancedEndless";
        public const string PLUGIN_NAME = "enhanced_endless";
        public const string PLUGIN_VERSION = "0.0.1";
    }
}
