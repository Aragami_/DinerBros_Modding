﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using BepInEx;
using HarmonyLib;
using UnityEngine;

namespace EnhancedEndless
{
    [BepInPlugin(PluginInfo.PLUGIN_GUID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
    [BepInProcess("Diner Bros.exe")]
    public class EnhancedEndlessPlugin : BaseUnityPlugin
    {
        public static EnhancedEndlessPlugin Instance { get; private set; }

        private readonly Harmony m_harmony = new Harmony("aragami.dinerbrosplugins.enhanced_endless");

        private void Awake()
        {
            Debug.Log("Here?");
            if (!Instance)
            {
                Instance = this;
            }
            
            m_harmony.PatchAll();
            Logger.LogInfo(PluginInfo.PLUGIN_GUID + " is loaded! (Build 04.03.2022");
        }
    }
    
    // [HarmonyPatch(typeof(WaiterControl), "checkIfDrinkRequired")]
    // public static class WaiterControlPatch
    // {
    //     // ReSharper disable once UnusedMember.Local
    //     static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> _instructions)
    //     {
    //         // Commented to be able to enabled it midgame later
    //         // if (!(PluginConfig.SFirstWaiter.Value || PluginConfig.SSecondWaiter.Value))
    //         //     return _instructions;
    //         // Check against known values on startup:
    //         // if (!PluginConfig.SServeDrinksMaxPlayers.Value)
    //         //     return _instructions;
    //
    //         var codes = new List<CodeInstruction>(_instructions);
    //         for (var i = 0; i < codes.Count; i++)
    //         {
    //             if (codes[i].opcode == OpCodes.Ldsfld && codes[i + 1].opcode == OpCodes.Ldc_I4_1 &&
    //                 codes[i + 2].opcode == OpCodes.Ble)
    //             {
    //                 codes[i + 1].opcode = OpCodes.Ldc_I4_4;
    //             }
    //         }
    //
    //         return codes.AsEnumerable();
    //     }
    // }
    
    [HarmonyPatch(typeof(WaiterControl), "Update")]
    // ReSharper disable once InconsistentNaming
    public static class WaiterControl_Update
    {
        [HarmonyPostfix]
        // ReSharper disable once InconsistentNaming
        // ReSharper disable once UnusedMember.Local
        static void StartPatch(ref WaiterControl __instance)
        {
            if (Game.campaign != null)
                Debug.Log(Game.campaign);
        }
    }
}
