﻿using BepInEx.Configuration;

namespace DinerBros_MultiplayerWaitersMod.DB_PluginConfig
{
    public static class PluginConfig
    {
        public static ConfigEntry<bool> SFirstWaiter;

        public static ConfigEntry<bool> SSecondWaiter;
        
        public static ConfigEntry<bool> SServeDrinks;
        
        public static ConfigEntry<bool> STakeOrdersAuto;
        
        public static ConfigEntry<int> SWaiterCounter1;
        
        public static ConfigEntry<int> SWaiterCounter2;

        // Weired 2 player Waiter limitation - No ingame config since i expect this to always be on
        public static ConfigEntry<bool> SRemove2PlayerWaiterLimitation;

        public static ConfigEntry<bool> SCleanDishesAuto;

        public static bool GameChangingConfigEnabled()
        {
            if (Game.playerCount <= 1) return false;
            // Waiters enabled
            if (SFirstWaiter.Value || SSecondWaiter.Value)
                return true;
            // Auto actions enabled
            if (STakeOrdersAuto.Value || SCleanDishesAuto.Value)
                return true;

            return false;
        }
    }
}