// TODO: Scrollbar doesn't always move, but sometimes:
//       Transpiler MainMenu.checkScrollPosition change 7.0 to option hierarchy elements divided by 2

using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using SunCubeStudio.Localization;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace DinerBros_MultiplayerWaitersMod.DB_PluginHelper
{
    public static class PluginHelper
    {
        public static bool IsChallengeMode(LevelController _instance)
        {
            FieldInfo myCustomLevelInfo =
                typeof(LevelController).GetField("customLevel", BindingFlags.NonPublic | BindingFlags.Instance);
            // ReSharper disable once PossibleNullReferenceException
            CustomLevel myCustomLevel = (CustomLevel)myCustomLevelInfo.GetValue(_instance);
            if (myCustomLevel != null && _instance.isCustomLevel &&
                (!myCustomLevel.endlessMode && !myCustomLevel.versusMode && !myCustomLevel.zombieMode))
                return true;
            return false;
        }

        public static bool SwapToFirstInArray<T>(ref T[] _array, int _positionToFirst)
        {
            // check for out of range
            if (_array.Length <= _positionToFirst) return false;

            // swap index x and 0
            T buffer = _array[0];
            _array[0] = _array[_positionToFirst];
            _array[_positionToFirst] = buffer;
            return true;
        }

        public static MainMenu GetMainMenu => Object.FindObjectOfType<MainMenu>();
    }

    public class EventTriggerOnSubmitMono : MonoBehaviour, ISelectHandler
    {
        public int SubmitId;

        public void OnSelect(BaseEventData _eventData)
        {
            PluginHelper.GetMainMenu.checkScrollPosition(SubmitId);
        }
    }

    public abstract class OptionsUiElement
    {
        private static readonly List<OptionsUiElement> SExistingElements = new List<OptionsUiElement>();
        protected static RectTransform SOptionsMenuElementParent;

        protected Transform OptionsUiElementTransform;
        private readonly OptionsUiElement[] m_disableOnFalse; // TODO: Remove optional hiding of options as it leads to more problems
        private readonly Text m_textComponent;

        protected OptionsUiElement(string _name/*, params
            OptionsUiElement[] _disableOnFalse*/)
        {
            // ReSharper disable once VirtualMemberCallInConstructor
            InstantiateElement();
            // ReSharper disable once AccessToStaticMemberViaDerivedType
            GameObject.Destroy(OptionsUiElementTransform.GetComponentInChildren<UILocalization>());
            m_textComponent = OptionsUiElementTransform.GetComponentInChildren<Text>();
            Text = _name;
            SExistingElements.Add(this);
            m_disableOnFalse = new OptionsUiElement[0];
        }

        protected abstract void InitChildVisibility();

        public static void InitAllChildVisibility()
        {
            foreach (OptionsUiElement uiElement in SExistingElements)
            {
                uiElement.InitChildVisibility();
            }
        }

        protected abstract void InstantiateElement();

        public static void SetElementParent(Transform _parent)
        {
            SOptionsMenuElementParent = _parent.GetComponent<RectTransform>();
            ContentSizeFitter fitter = SOptionsMenuElementParent.gameObject.AddComponent<ContentSizeFitter>();
            fitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;

            foreach (RectTransform child in SOptionsMenuElementParent.Cast<RectTransform>()
                .OrderBy(_t => _t.GetSiblingIndex()))
            {
                EventTrigger[] eventTriggers = child.GetComponentsInChildren<EventTrigger>();
                foreach (EventTrigger eventTrigger in eventTriggers)
                {
                    EventTriggerOnSubmitMono temp = eventTrigger.gameObject.AddComponent<EventTriggerOnSubmitMono>();
                    temp.SubmitId = child.GetSiblingIndex() + 1;
                }
            }
        }

        public static void UpdateScrollData()
        {
            foreach (RectTransform child in SOptionsMenuElementParent.Cast<RectTransform>()
                .OrderBy(_t => _t.GetSiblingIndex()))
            {
                EventTriggerOnSubmitMono[] submitArray = child.GetComponentsInChildren<EventTriggerOnSubmitMono>();
                foreach (EventTriggerOnSubmitMono submitMono in submitArray)
                {
                    submitMono.SubmitId = child.GetSiblingIndex() + 1;
                }
            }
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public bool Visible
        {
            get => OptionsUiElementTransform.gameObject.activeSelf;
            set
            {
                // TODO: Controller/Keyboard up and down, remove on disabled and add on enabled 
                // Only if not already false, to avoid loops
                if (value != OptionsUiElementTransform.gameObject.activeSelf)
                {
                    //DisableOthersOnFalse(value);

                    OptionsUiElementTransform.gameObject.SetActive(value);

                    UpdateScrollData();
                }
            }
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public string Text
        {
            get => m_textComponent.text;
            set => m_textComponent.text = value;
        }

        protected void DisableOthersOnFalse(bool _disableOnFalse)
        {
            // Make depending toggles invisible
            // ReSharper disable once MergeIntoPattern | Easier to read
            if (m_disableOnFalse != null && m_disableOnFalse.Length > 0)
            {
                foreach (OptionsUiElement childElement in m_disableOnFalse)
                {
                    childElement.Visible = _disableOnFalse;
                }
            }
        }

        // TODO: OrderIndex also changes toggle/radioButton
        public int OrderIndex
        {
            get => OptionsUiElementTransform.GetSiblingIndex();
            set
            {
                OptionsUiElementTransform.SetSiblingIndex(value);

                Selectable[] previousSelectables = new Selectable[] { };
                Selectable[] nextSelectables = new Selectable[] { };

                // Prev
                Transform previousSibling = GetPreviousSibling(OptionsUiElementTransform);
                if (previousSibling != null)
                {
                    // On Radio button it gets all buttons it finds, for toggle only one, same for button
                    // This will also take the reset button, they need to be selected via mouse
                    previousSelectables = previousSibling.GetComponentsInChildren<Selectable>();

                    if (previousSelectables != null && previousSelectables.Length > 0)
                    {
                        foreach (Selectable selectable in previousSelectables)
                        {
                            Navigation newNav = selectable.navigation;
                            newNav.selectOnDown = GetSelectable;
                            selectable.navigation = newNav;
                        }
                    }
                }

                // Next
                Transform nextSibling = GetNextSibling(OptionsUiElementTransform);
                if (nextSibling != null)
                {
                    // On Radio button it gets all buttons it finds, for toggle only one, same for button
                    // This will also take the reset button, they need to be selected via mouse
                    nextSelectables = nextSibling.GetComponentsInChildren<Selectable>();

                    if (nextSelectables != null && nextSelectables.Length > 0)
                    {
                        foreach (Selectable selectable in nextSelectables)
                        {
                            Navigation newNav = selectable.navigation;
                            newNav.selectOnUp = GetSelectable;
                            selectable.navigation = newNav;
                        }
                    }
                }

                // Own
                SetOwnSelectables(previousSelectables, nextSelectables);

                UpdateScrollData();
            }
        }

        protected abstract void SetOwnSelectables(Selectable[] _prevs, Selectable[] _next);

        private static Transform SiblingAtIndex(Transform _transform, int _index)
        {
            return _transform.parent.GetChild(_index);
        }

        private static Transform GetPreviousSibling(Transform _transform)
        {
            if (_transform.GetSiblingIndex() > 0)
            {
                for (int i = _transform.GetSiblingIndex() - 1; i >= 0; i--)
                {
                    if (SiblingAtIndex(_transform, i).gameObject.activeSelf)
                    {
                        return SiblingAtIndex(_transform, i);
                    }
                }
            }

            return null;
        }

        private static Transform GetNextSibling(Transform _transform)
        {
            if (_transform.parent.childCount - 1 > _transform.GetSiblingIndex())
            {
                for (int i = _transform.GetSiblingIndex() + 1; i < _transform.parent.childCount; i++)
                {
                    if (SiblingAtIndex(_transform, i).gameObject.activeSelf)
                    {
                        return SiblingAtIndex(_transform, i);
                    }
                }
            }

            return null;
        }

        public abstract Selectable GetSelectable { get; }
    }

    public class OptionsUiToggle : OptionsUiElement
    {
        private static Transform SOptionsMenuToggleOriginal;


        // private static bool s_nextWithFrame = false;

        public static void AddOriginal(Transform _optionsUiToggle)
        {
            SOptionsMenuToggleOriginal = _optionsUiToggle;
        }

        private readonly Toggle m_toggle;
        private readonly bool m_beginningState;
        
        private readonly UnityAction<bool> m_toggleChanged;

        public OptionsUiToggle(string _name, bool _defaultState, UnityAction<bool> _onValueChanged/*, params
            OptionsUiElement[] _disableOnFalse*/) : base(_name/*, _disableOnFalse*/)
        {
            m_toggle = OptionsUiElementTransform.GetComponentInChildren<Toggle>();
            m_beginningState = _defaultState;
            // ReSharper disable once VirtualMemberCallInConstructor - Values needed are added in this constructor
            InitChildVisibility();
            m_toggle.onValueChanged = new Toggle.ToggleEvent(); /*RemoveAllListeners();*/
            m_toggleChanged += _onValueChanged;
            AddListener(OnToggleValueChanged);
            // AddListener(DisableOthersOnFalse);
            // Try to make the background change each option - didn't work, but not worth investigating
            // if (s_nextWithFrame)
            //     m_optionsUiToggle.Find("framing").gameObject.SetActive(s_nextWithFrame);
            // s_nextWithFrame = !s_nextWithFrame;
        }

        private void OnToggleValueChanged(bool _changed)
        {
            m_toggleChanged.Invoke(GetToggleValue);
        }
        
        private bool GetToggleValue
        {
            get
            {
                return m_toggle.isOn;
            }
        }
        
        // ReSharper disable once MemberCanBePrivate.Global
        public void AddListener(UnityAction<bool> _onValueChanged)
        {
            m_toggle.onValueChanged.AddListener(_onValueChanged);
        }

        protected override void InitChildVisibility()
        {
            m_toggle.isOn = m_beginningState;
            //DisableOthersOnFalse(m_beginningState);
        }

        protected override void InstantiateElement()
        {
            // ReSharper disable once AccessToStaticMemberViaDerivedType
            OptionsUiElementTransform = GameObject.Instantiate(SOptionsMenuToggleOriginal, SOptionsMenuElementParent);
        }

        protected override void SetOwnSelectables(Selectable[] _prevs, Selectable[] _next)
        {
            Navigation newNav = m_toggle.navigation;
            if (_prevs.Length > 0)
                newNav.selectOnUp = _prevs[0];
            if (_next.Length > 0)
                newNav.selectOnDown = _next[0];
            m_toggle.navigation = newNav;

            // TODO: Move scrollbar with toggle up and down
        }

        public override Selectable GetSelectable => m_toggle;
    }

    public class OptionsUiRadio : OptionsUiElement
    {
        private static Transform SOptionsMenuRadioOriginal;


        // private static bool s_nextWithFrame = false;

        public static void AddOriginal(Transform _optionsMenuRadio)
        {
            SOptionsMenuRadioOriginal = _optionsMenuRadio;
        }

        private readonly Toggle m_toggle0;
        private readonly Toggle m_toggle1;
        private readonly Toggle m_toggle2;
        private readonly int m_beginningState;

        private readonly UnityAction<int> m_radicChanged;

        public OptionsUiRadio(string _name, int _defaultState, UnityAction<int> _onValueChanged,
            bool _removeSmileys = true/*, params
                OptionsUiElement[] _disableOnFalse*/) : base(_name/*, _disableOnFalse*/)
        {
            Transform toggleGroup = OptionsUiElementTransform.Find("ToggleGroup");
            m_toggle0 = toggleGroup.Find("radio").GetComponent<Toggle>();
            // ReSharper disable StringLiteralTypo | The name of the GameObject has a typo so Find() also needs the typo
            m_toggle1 = toggleGroup.Find("optonsToggle2").GetComponent<Toggle>();
            m_toggle2 = toggleGroup.Find("optonsToggle3").GetComponent<Toggle>();
            // ReSharper restore StringLiteralTypo
            // Compared to RemoveAllListeners(), new ToggleEvent will also get rid of all persistent listeners
            m_toggle0.onValueChanged = new Toggle.ToggleEvent(); /*.RemoveAllListeners();*/
            m_toggle1.onValueChanged = new Toggle.ToggleEvent(); /*.RemoveAllListeners();*/
            m_toggle2.onValueChanged = new Toggle.ToggleEvent(); /*.RemoveAllListeners();*/
            m_beginningState = _defaultState;
            // ReSharper disable once VirtualMemberCallInConstructor - Values needed are added in this constructor
            InitChildVisibility();
            m_radicChanged += _onValueChanged;
            AddListener(OnRadioValueChanged);
            if (_removeSmileys)
            {
                Image[] images = OptionsUiElementTransform.GetComponentsInChildren<Image>();
                foreach (Image image in images)
                {
                    if (image.transform.name == "iconFrown" || image.transform.name == "iconSmile")
                    {
                        // ReSharper disable once AccessToStaticMemberViaDerivedType
                        GameObject.Destroy(image.gameObject);
                    }
                }
            }
            // Try to make the background change each option - didn't work, but not worth investigating
            // if (s_nextWithFrame)
            //     m_optionsUiToggle.Find("framing").gameObject.SetActive(s_nextWithFrame);
            // s_nextWithFrame = !s_nextWithFrame;
        }

        private void OnRadioValueChanged(bool _changed)
        {
            m_radicChanged.Invoke(GetRadioValue);
        }

        private int GetRadioValue
        {
            get
            {
                if (m_toggle0.isOn)
                    return 0;
                if (m_toggle1.isOn)
                    return 1;
                if (m_toggle2.isOn)
                    return 2;
                return 0;
            }
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public void AddListener(UnityAction<bool> _onValueChanged)
        {
            m_toggle0.onValueChanged.AddListener(_onValueChanged);
            m_toggle1.onValueChanged.AddListener(_onValueChanged);
            m_toggle2.onValueChanged.AddListener(_onValueChanged);
        }

        protected override void InitChildVisibility()
        {
            switch (m_beginningState)
            {
                case 0:
                    m_toggle0.isOn = true;
                    m_toggle1.isOn = false;
                    m_toggle2.isOn = false;
                    break;
                case 1:
                    m_toggle0.isOn = false;
                    m_toggle1.isOn = true;
                    m_toggle2.isOn = false;
                    break;
                case 2:
                    m_toggle0.isOn = false;
                    m_toggle1.isOn = false;
                    m_toggle2.isOn = true;
                    break;
            }
        }

        protected override void InstantiateElement()
        {
            // ReSharper disable once AccessToStaticMemberViaDerivedType
            OptionsUiElementTransform = GameObject.Instantiate(SOptionsMenuRadioOriginal, SOptionsMenuElementParent);
        }

        // ReSharper disable once IdentifierTypo
        protected override void SetOwnSelectables(Selectable[] _prevs, Selectable[] _next)
        {
            Navigation newNav0 = m_toggle0.navigation;
            if (_prevs.Length > 0)
                newNav0.selectOnUp = _prevs[0];
            if (_next.Length > 0)
                newNav0.selectOnDown = _next[0];
            m_toggle0.navigation = newNav0;

            Navigation newNav1 = m_toggle1.navigation;
            if (_prevs.Length > 0)
                newNav1.selectOnUp = _prevs[0];
            if (_next.Length > 0)
                newNav1.selectOnDown = _next[0];
            m_toggle1.navigation = newNav1;

            Navigation newNav2 = m_toggle2.navigation;
            if (_prevs.Length > 0)
                newNav2.selectOnUp = _prevs[0];
            if (_next.Length > 0)
                newNav2.selectOnDown = _next[0];
            m_toggle2.navigation = newNav2;
        }

        public override Selectable GetSelectable
        {
            get
            {
                if (m_toggle0.isOn)
                    return m_toggle0;
                if (m_toggle1.isOn)
                    return m_toggle1;
                if (m_toggle2.isOn)
                    return m_toggle2;
                return m_toggle0;
            }
        }
    }
}