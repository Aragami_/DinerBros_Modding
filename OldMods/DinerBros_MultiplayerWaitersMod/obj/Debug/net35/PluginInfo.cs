namespace DinerBros_MultiplayerWaitersMod
{
    public static class PluginInfo
    {
        public const string PLUGIN_GUID = "DinerBros_MultiplayerWaitersMod";
        public const string PLUGIN_NAME = "DinerBros_MultiplayerWaitersMod";
        public const string PLUGIN_VERSION = "1.0.0";
    }
}
