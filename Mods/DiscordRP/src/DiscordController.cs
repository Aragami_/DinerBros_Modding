﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Discord;
using UnityEngine.SceneManagement;

namespace DiscordRP
{
    public enum GAMEMODE
    {
        NONE = 0,
        CAMPAIGN,
        CHALLENGE,
        VERSUS,
        ENDLESS
    }

    public enum MAP
    {
        NONE = 0,
        BURGER,
        PIZZA,
        SUSHI,
        TACO,
        ZOMBIE,
        MENU,
        UPGRADE
    }

    public class DiscordController : MonoBehaviour
    {
        public Discord.Discord m_discord;
        private ActivityManager m_manager;
        private Activity m_activity;
        public GAMEMODE m_gamemode = GAMEMODE.NONE;
        public MAP m_map = MAP.NONE;
        public int m_customersServed = 0;
        public long m_startTimeStamp;
        public long m_endTimeStamp;

        private GameController m_gameController;
        private CustomLevel m_customLevel;

        private const string m_iconCampaign = "ui_icon_campaign";
        private const string m_textCampaign = "Campaign";
        private const string m_iconChallenge = "ui_icon_challenge";
        private const string m_textChallenge = "Challenge";
        private const string m_iconVersus = "ui_icon_versus";
        private const string m_textVersus = "Versus";
        private const string m_iconEndless = "ui_icon_endless";
        private const string m_textEndless = "Endless";
        private const string m_iconMenu = "";
        private const string m_textMenu = "";

        private const string m_iconBurgerBros = "";
        private const string m_textBurgerBros = "Burger Bros";
        private const string m_iconPizzaBros = "";
        private const string m_textPizzaBros = "Pizza Bros";
        private const string m_iconSushiBros = "";
        private const string m_textSushiBros = "Sushi Bros";
        private const string m_iconTacoAmigos = "logo_taco";
        private const string m_textTacoAmigos = "Taco Amigos";
        private const string m_iconZombies = "";
        private const string m_textZombies = "Zombies";


        // Use this for initialization
        void Awake()
        {
            DontDestroyOnLoad(this);

            m_discord = new Discord.Discord(960151657512063048, (System.UInt64)Discord.CreateFlags.NoRequireDiscord);
            m_manager = m_discord.GetActivityManager();
            m_activity = new Discord.Activity
            {
                State = "Playing something",
                Details = "This is a description for the obvious title",
            };
            m_activity.Party.Size.CurrentSize = 1;
            m_activity.Party.Size.MaxSize = 4;
        }

        // Update is called once per frame
        void Update()
        {
            m_discord.RunCallbacks();
        }
        #region Scenes

        private void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        private void OnSceneLoaded(Scene _scene, LoadSceneMode _mode)
        {
            // ReSharper disable StringLiteralTypo
            if (_mode == LoadSceneMode.Single)
            {
                switch (_scene.name)
                {
                    case "MainMenu":
                        m_gamemode = GAMEMODE.NONE;
                        m_map = MAP.MENU;
                        break;
                    case "UpgradeScreen":
                        m_gamemode = GAMEMODE.CAMPAIGN;
                        m_map = MAP.UPGRADE;
                        break;
                    case "Level_1":
                    case "Level_2":
                    case "Level_3":
                    case "Level_Tutorial":
                        m_gamemode = GAMEMODE.CAMPAIGN;
                        m_map = MAP.BURGER;
                        break;
                    case "Level_P1":
                    case "Level_P2":
                    case "Level_P3":
                        m_gamemode = GAMEMODE.CAMPAIGN;
                        m_map = MAP.PIZZA;
                        break;
                    case "Level_S1":
                    case "Level_S2":
                    case "Level_S3":
                        m_gamemode = GAMEMODE.CAMPAIGN;
                        m_map = MAP.SUSHI;
                        break;
                    case "Level_T1":
                    case "Level_T2":
                    case "Level_T3":
                        m_gamemode = GAMEMODE.CAMPAIGN;
                        m_map = MAP.TACO;
                        break;
                    case "Level_VBurg":
                        m_gamemode = GAMEMODE.VERSUS;
                        m_map = MAP.BURGER;
                        break;
                    case "Level_VPizz":
                        m_gamemode = GAMEMODE.VERSUS;
                        m_map = MAP.PIZZA;
                        break;
                    case "Level_3Endless":
                        m_gamemode = GAMEMODE.ENDLESS;
                        m_map = MAP.BURGER;
                        break;
                    case "Level_P3Endless":
                        m_gamemode = GAMEMODE.ENDLESS;
                        m_map = MAP.PIZZA;
                        break;
                    case "Level_S3Endless":
                        m_gamemode = GAMEMODE.ENDLESS;
                        m_map = MAP.SUSHI;
                        break;
                    case "Level_T3Endless":
                        m_gamemode = GAMEMODE.ENDLESS;
                        m_map = MAP.TACO;
                        break;
                    case "Level_ZombieEndless":
                        m_gamemode = GAMEMODE.ENDLESS;
                        m_map = MAP.ZOMBIE;
                        break;
                    default:
                        m_gamemode = GAMEMODE.CHALLENGE;
                        m_map = MAP.NONE;
                        break;
                }
                // ReSharper restore StringLiteralTypo
            }

            #region Time

            m_startTimeStamp = (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds; // Not perfectly accurate, since the day starts a few seconds later, but good enough without callbacks/harmony

            if (m_gamemode == GAMEMODE.CAMPAIGN && m_map != MAP.UPGRADE)
            {
                int dayDuration;
                if (Game.day <= 1)
                    dayDuration = 90;
                else if (Game.day <= 2.0)
                    dayDuration = 120;
                else if (Game.day <= 3.0)
                    dayDuration = 180;
                else if (m_map == MAP.PIZZA && (double)Game.day > 12.0 ||
                         m_map == MAP.SUSHI && (double)Game.day > 16.0 ||
                         m_map == MAP.TACO && (double)Game.day > 3.0)
                    dayDuration = 300;
                else
                    dayDuration = 240;
                m_endTimeStamp = m_startTimeStamp + dayDuration;
            }
            else
            {
                m_endTimeStamp = m_startTimeStamp;
            }

            #endregion

            // Cancel and make sure it gets called immediately
            CancelInvoke(nameof(UpdateDiscordRp));
            InvokeRepeating(nameof(UpdateDiscordRp), 0f, 15f);
        }

        #endregion

        public int GetCustomerCount
        {
            get
            {
                if (m_gameController == null)
                {
                    m_gameController = FindObjectOfType<GameController>();
                }
                return m_gameController != null ? (int)m_gameController.customersServed : -1;
            }
        }

        public int GetEndlessCurrentLives
        {
            get
            {
                if (m_customLevel == null)
                {
                    m_customLevel = FindObjectOfType<CustomLevel>();
                }
                return m_customLevel != null ? m_customLevel.currentLives : -1;
            }
        }

        public int GetEndlessMaxLives
        {
            get
            {
                if (m_customLevel == null)
                {
                    m_customLevel = FindObjectOfType<CustomLevel>();
                }
                return m_customLevel != null ? m_customLevel.maxLives : -1;
            }
        }

        public void UpdateDiscordRp()
        {
            string state;
            string smallImage;
            string smallText;
            string largeImage;
            string largeText;

            switch (m_gamemode)
            {
                case GAMEMODE.CAMPAIGN:
                    state = m_map == MAP.UPGRADE ? "Upgrading" : "Campaign on ";
                    smallImage = m_iconCampaign;
                    smallText = m_textCampaign;
                    break;
                case GAMEMODE.CHALLENGE:
                    state = "Challenge mode";
                    smallImage = m_iconChallenge;
                    smallText = m_textChallenge;
                    break;
                case GAMEMODE.VERSUS:
                    state = "Versus on ";
                    smallImage = m_iconVersus;
                    smallText = m_textVersus;
                    break;
                case GAMEMODE.ENDLESS:
                    state = "Endless on ";
                    smallImage = m_iconEndless;
                    smallText = m_textEndless;
                    break;
                case GAMEMODE.NONE:
                default:
                    state = "In Menus";
                    smallImage = m_iconMenu;
                    smallText = m_textMenu;
                    break;
            }

            switch (m_map)
            {
                case MAP.BURGER:
                    state += "Burger Bros";
                    largeText = m_textBurgerBros;
                    break;
                case MAP.PIZZA:
                    state += "Pizza Bros";
                    largeText = m_textPizzaBros;
                    break;
                case MAP.SUSHI:
                    state += "Sushi Bros";
                    largeText = m_textSushiBros;
                    break;
                case MAP.TACO:
                    state += "Taco Amigos";
                    largeText = m_textTacoAmigos;
                    break;
                case MAP.ZOMBIE:
                    state += "Zombies";
                    largeText = m_textZombies;
                    break;
                case MAP.MENU:
                case MAP.NONE:
                default:
                    largeText = "Main Menu";
                    break;
            }

            m_activity.Assets.SmallImage = smallImage;
            m_activity.Assets.SmallText = smallText;
            m_activity.Assets.LargeImage = m_iconTacoAmigos;
            m_activity.Assets.LargeText = largeText;
            m_activity.State = state;
            int customers = GetCustomerCount;
            string details = customers != -1 ? "Customers: " + customers.ToString() : "";
            if (m_gamemode == GAMEMODE.ENDLESS)
            {
                int lives = GetEndlessCurrentLives;
                details += lives != -1 ? " | Lives: " + lives.ToString() + "/" + GetEndlessMaxLives.ToString() : "";
            }
            m_activity.Details = details;
            m_activity.Party.Size.CurrentSize = Game.playerCount;
            m_activity.Timestamps = new ActivityTimestamps {Start = m_startTimeStamp};
            if (m_startTimeStamp != m_endTimeStamp)
            {
                m_activity.Timestamps.End = m_endTimeStamp;
            }

            m_manager.UpdateActivity(m_activity, (res) =>
            {
                if (res == Discord.Result.Ok)
                {
                    Debug.LogWarning("Everything is fine!");
                }
            });
        }
    }
}
