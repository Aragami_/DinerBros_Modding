﻿using BepInEx;
using UnityEngine;

namespace DiscordRP
{
    [BepInPlugin(PluginInfo.PLUGIN_GUID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
    public class Plugin : BaseUnityPlugin
    {
        private DiscordController m_discordController;

        private void Awake()
        {
            // Plugin startup logic
            Logger.LogInfo($"Plugin {PluginInfo.PLUGIN_GUID} is loaded!");

            m_discordController = new GameObject("DiscordController").AddComponent<DiscordController>();
        }

        public DiscordController DiscordController => m_discordController;
    }
}
