﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using BepInEx;
using HarmonyLib;
using UnityEngine;
using ModHelper;
using Rewired;

namespace EnhancedEndless
{
    [BepInPlugin(PluginInfo.PLUGIN_GUID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
    [BepInProcess("Diner Bros.exe")]
    public class EnhancedEndless : BaseUnityPlugin
    {
        public static EnhancedEndless Instance { get; private set; }
        public LevelController LevelController;

        private readonly Harmony m_harmony = new Harmony("aragami.dinerbrosplugins.EnhancedEndless");
        private void Awake()
        {
            if (!Instance)
            {
                Instance = this;
            }

            m_harmony.PatchAll();
            Logger.LogInfo($"Plugin {PluginInfo.PLUGIN_GUID} is loaded!");
        }

        #region Players
        /// <summary>
        /// Finds a character name that has not yet been used by another player
        /// </summary>
        /// <returns>Character name in resource files</returns>
        public static string FirstUnusedCharString
        {
            get
            {
                List<PlayerControl> playerGOs = (from a
                        in FindObjectOfType<GameController>().playerGOs
                                                 select a.gameObject.GetComponent<PlayerControl>()).ToList();
                for (int i = 0; i < MainMenu.possibleChars.Length; i++)
                {
                    bool found = false;
                    foreach (PlayerControl playerControl in playerGOs)
                    {
                        if (i == playerControl.characterType)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        return MainMenu.possibleChars[i];
                    }
                }

                return MainMenu.possibleChars[0];
            }
        }

        public static int CurrentAmountOfControllerPlayers
        {
            get
            {
                int count = 0;
                foreach (int i in Game.rewiredPlayer)
                {
                    // Not unassigned and not a keyboard player
                    if (i != 99 && i != 0)
                    {
                        count++;
                    }
                }

                return count;
            }
        }

    }

    [HarmonyPatch(typeof(LevelController), "Update")]
    public static class LevelControllerUpdatePatch
    {
        [HarmonyPostfix]
        // ReSharper disable once UnusedMember.Local
        static void StartPatch(ref LevelController __instance, GameController ___gameController)
        {
            {
                bool keyboardInUse = false;
                bool keyboardPressed = false;
                bool joinPressed = false;
                if (Input.GetKeyDown(KeyCode.O))
                {
                    foreach (int i in Game.rewiredPlayer.Where(_i => _i == 0))
                    {
                        keyboardInUse = true;
                    }

                    // Only add keyboard player when keyboard not in use yet
                    if (!keyboardInUse)
                    {
                        Debug.Log("Adding Keyboard player");
                        keyboardPressed = true;
                        joinPressed = true;
                    }
                }

                if (Input.GetKeyDown(KeyCode.P))
                {
                    foreach (int i in Game.rewiredPlayer.Where(_i => _i == 0))
                    {
                        keyboardInUse = true;
                    }
                    Debug.Log("Adding Controller player");
                    joinPressed = true;
                }
                if (joinPressed)
                {
                    int newPlayerIndex = 0;
                    if (Game.rewiredPlayer[1] == 99)
                    {
                        newPlayerIndex = 1;
                    }
                    else if (Game.rewiredPlayer[2] == 99)
                    {
                        newPlayerIndex = 2;
                    }
                    else if (Game.rewiredPlayer[3] == 99)
                    {
                        newPlayerIndex = 3;
                    }
                    else if (Game.rewiredPlayer[4] == 99)
                    {
                        newPlayerIndex = 4;
                    }

                    if (newPlayerIndex != 0)
                    {
                        Player newPlayer = ReInput.players.GetPlayer(keyboardInUse ? newPlayerIndex : (keyboardPressed ? 0 : newPlayerIndex + 1));
                        {
                            Game.rewiredPlayer[newPlayerIndex] = keyboardInUse ? newPlayerIndex : (keyboardPressed ? 0 : newPlayerIndex + 1);
                            // Create Player has 1-4; Player 0 is System(I think its for UI control; Player 1 has index 0;
                            ___gameController.createPlayer(newPlayerIndex + 1, EnhancedEndless.FirstUnusedCharString, Game.rewiredPlayer[newPlayerIndex]);
                        }
                    }
                    #region Log
                    Debug.Log("Late: newPlayerIndex: " + newPlayerIndex + "; keyboardInUse: " + keyboardInUse + "; keyboardPressed: "
                              + keyboardPressed + "; Game.rewiredPlayer[newPlayerIndex]: " + Game.rewiredPlayer[newPlayerIndex]);
                    for (int index = 0; index < Game.rewiredPlayer.Count; index++)
                    {
                        Debug.Log("RewiredPlayer (" + index + "): " + Game.rewiredPlayer[index]);
                    }
                    #endregion
                }
            }
        }
    }

    [HarmonyPatch(typeof(ItemScript), "Awake")]
    public static class ItemScriptAwakePatch
    {
        [HarmonyPrefix]
        // ReSharper disable once UnusedMember.Local
        static void StartPatch(ref bool[] ___playernumInteract)
        {
            ___playernumInteract = new bool[6];
        }
    }

    [HarmonyPatch(typeof(Game), "setPlayerInputs")]
    public static class GameSetPlayerInputsPatch
    {
        [HarmonyPrefix]
        // ReSharper disable once UnusedMember.Local
        static void StartPatch()
        {
            if (Game.playerInputs.Count < 5)
                Game.playerInputs.Add(string.Empty);
            if (Game.rewiredPlayer.Count < 5)
                Game.rewiredPlayer.Add(99);
            if (Game.playerLastCharInt.Count < 5)
                Game.playerLastCharInt.Add(-1);
            if (Game.playerCharacters.Count < 5)
                Game.playerCharacters.Add(string.Empty);
        }
    }

    [HarmonyPatch(typeof(MainMenu), "Awake")]
    public static class MainMenuAwakePatch
    {
        [HarmonyPostfix]
        // ReSharper disable once UnusedMember.Local
        static void StartPatch(ref int[] ___playerInputs, ref string[] ___playerCharacter, ref bool[] ___playerReady, ref int[] ___playerCharacterid)
        {
            ___playerInputs = new int[5] { 99, 99, 99, 99, 99 };
            ___playerCharacter = new string[5] { string.Empty, string.Empty, string.Empty, string.Empty, string.Empty };
            ___playerReady = new bool[5];
            ___playerCharacterid = new int[5] { 0, 1, 2, 3, 4 };
        }
    }

    [HarmonyPatch(typeof(GameController), "findPlayerSpawn")]
    public static class GameControllerFindPlayerSpawnPatch
    {
        [HarmonyPostfix]
        // ReSharper disable once UnusedMember.Local
        static void StartPatch(ref GameController __instance, ref List<Vector3> ___playerSpawn)
        {
            ___playerSpawn.Add(___playerSpawn[0]); // 5th spawn same as first one
        }
    }

    #endregion

    [HarmonyPatch(typeof(AudioController), "speedMusic")]
    public static class AudioControllerPatch
    {
        [HarmonyPostfix]
        // ReSharper disable once UnusedMember.Local
        static void StartPatch(ref AudioController __instance, ref IEnumerator ___fadePitchCR)
        {
            if (EnhancedEndless.Instance.LevelController &&
                EnhancedEndless.Instance.LevelController.isEndlessMode &&
                !MHFunctions.IsChallengeMode(EnhancedEndless.Instance.LevelController) &&
                EnhancedEndless.Instance.LevelController.isCustomLevel)
            {
                __instance.StopCoroutine(___fadePitchCR);
                ___fadePitchCR = __instance.fadePitch(0.008f);
                __instance.StartCoroutine(___fadePitchCR);
            }
        }
    }

    //public class CustomMono : MonoBehaviour
    //{
    //    public void OnDisable()
    //    {
    //        //EnhancedEndless.Instance.Hjdata.player2Joined = false;
    //    }
    //}

    [HarmonyPatch(typeof(LevelController), "Start")]
    public static class LevelControllerStartPatch
    {
        [HarmonyPostfix]
        // ReSharper disable once UnusedMember.Local
        static void StartPatch(ref LevelController __instance, ref UIController ___uiController)
        {
            EnhancedEndless.Instance.LevelController = __instance;

            if (__instance.isEndlessMode && !MHFunctions.IsChallengeMode(__instance) && !__instance.isCustomLevel)
                ___uiController.setTime(99999f);
            //__instance.gameObject.AddComponent(typeof(CustomMono));
        }
    }


}
