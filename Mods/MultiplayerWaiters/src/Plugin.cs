﻿// Done: Where Waiters start for each map
// Done: More waiters limited by upgrades - untested
// Done: In-game option to disable this mod (no need to restart) (also drinks check?)
// TODO: Remove getting scores with waiters in multiplayer / achievements
// TODO: Buy waiter upgrades in campaign multiplayer
// Done: Single waiter set side


using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using ModHelper;
using ModHelper.MHUi;
using UnityEngine;
using static HarmonyLib.AccessTools;

namespace MultiplayerWaiters
{
    #region Plugin_Patching

    [BepInPlugin(PluginInfo.PLUGIN_GUID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
    [BepInDependency("ModHelper")]
    [BepInProcess("Diner Bros.exe")]
    public class MultiplayerWaiterPlugin : BaseUnityPlugin
    {
        public static MultiplayerWaiterPlugin Instance { get; private set; }

        private readonly Harmony m_harmony = new Harmony("aragami.dinerbrosplugins.multiplayer_waiters");

        private void Awake()
        {
            if (!Instance)
            {
                Instance = this;
            }

            #region Config

            PluginConfig.SFirstWaiter = Config.Bind(
                "Waiters.First",
                "FirstWaiter",
                true,
                new ConfigDescription("Enables the first waiter (The one that is bought as first in the campaign."));

            PluginConfig.SSecondWaiter = Config.Bind(
                "Waiters.Second",
                "SecondWaiter",
                true,
                new ConfigDescription("Enables the second waiter (The one that is bought as second in the campaign."));

            PluginConfig.SServeDrinks = Config.Bind(
                "Waiters.Behaviour",
                "ServerDrinks",
                true,
                "Allows waiters to serve drinks. Requires restart!");

            PluginConfig.STakeOrdersAuto = Config.Bind(
                "Waiters.Behaviour",
                "TakeOrdersAuto",
                false,
                new ConfigDescription(
                    "If enabled then the customers take orders by themself."));

            PluginConfig.SWaiterCounter1 = Config.Bind(
                "Waiters.First",
                "WaiterCounterFirst",
                0,
                new ConfigDescription(
                    "This will set next to which counter the first waiter will stand by default. In endless mode there are always 3 counters on every map.",
                    new AcceptableValueList<int>(0, 1, 2)));

            PluginConfig.SWaiterCounter2 = Config.Bind(
                "Waiters.Second",
                "WaiterCounterSecond",
                0,
                new ConfigDescription(
                    "This will set next to which counter the second waiter will stand by default. In endless mode there are always 3 counters on every map.",
                    new AcceptableValueList<int>(0, 1, 2)));

            PluginConfig.SRemove2PlayerWaiterLimitation = Config.Bind(
                "Waiters.Behaviour",
                "Remove2PlayerWaiterLimitation",
                true,
                "For some reason the waiters will only pick up items from a single counter if there are exactly 2 players.\nThis removes this limitation.");

            PluginConfig.SCleanDishesAuto = Config.Bind(
                "Waiters.Behaviour",
                "CleanDishesAuto",
                false,
                new ConfigDescription(
                    "If enabled then the customers clean dishes by themself."));

            #endregion

            m_harmony.PatchAll();
            Logger.LogInfo(PluginInfo.PLUGIN_GUID + " is loaded! (Build 04.03.2022");
        }

        public void BindConfig<T>(ConfigEntry<T> _config)
        {
            Config.Bind(_config.Definition, _config.Value, _config.Description);
        }
    }

    #endregion

    #region TestStuff

    // [HarmonyPatch(typeof(LevelController), "Start")]
    // public class MultipleWaitersMod
    // {
    //     [HarmonyPrefix]
    //     static void AddWaiterIfMultiplayer(ref GameController _gameController, ref LevelController _instance)
    //     {
    //         // if (__instance.playerCount >= 2)
    //         // {
    //         _gameController.createWaiter(1, "WaiterFemaleServer");
    //         _gameController.createWaiter(2, "WaiterFemaleServer");
    //         // ___gameController.createWaiter(1, "WaiterFemaleServer");
    //         // ___gameController.createWaiter(2, "WaiterFemaleServer");
    //         // ___gameController.createWaiter(1, "WaiterFemaleServer");
    //         // ___gameController.createWaiter(2, "WaiterFemaleServer");
    //         // ___gameController.createWaiter(1, "WaiterFemaleServer");
    //         // ___gameController.createWaiter(2, "WaiterFemaleServer");
    //         // ___gameController.createWaiter(1, "WaiterFemaleServer");
    //         // ___gameController.createWaiter(2, "WaiterFemaleServer");
    //         // }
    //
    //         Debug.Log(_instance.playerCount.ToString() + " Players");
    //     }
    // }

    //// notTODO: Only works when more than 2 waiters: Otherwise it will be inline and therefore ignored
    // [HarmonyPatch(typeof(GameController), nameof(GameController.getWaiterSpawn))]
    // public class WaiterSpawnMod
    // {
    //     [HarmonyPrefix]
    //     // ReSharper disable once InconsistentNaming
    //     static bool GiveWaiterSpawnPos(ref GameController __instance, ref List<Vector3> ___waiterSpawn, ref int __n,
    //         ref Vector3 __result)
    //     {
    //         __result = ___waiterSpawn[__n % 2];
    //         if (__n >= 2)
    //             Debug.Log("Spawnpoint reduziert");
    //         else
    //         {
    //             Debug.Log("Hat nicht geklappt!");
    //         }
    //         return false;
    //     }
    // }

    #endregion

    #region WaitersSpawn

    [HarmonyPatch(typeof(LevelController), "Start")]
    public static class LevelControllerStartPatch
    {
        [HarmonyPostfix]
        static void StartPatch(LevelController __instance, CustomLevel ___customLevel, GameController ___gameController)
        {
            if (PluginConfig.SFirstWaiter.Value &&
                Game.playerCount > 1 &&
                // Game statement:
                (UpgradeScreen.checkIfUpgraded("sa") &&
                 (double)LevelController.currentLevel >= 2.0 &&
                 !__instance.isCustomLevel ||
                 __instance.isCustomLevel && ___customLevel.waiter1)
                 )
            {
                ___gameController.createWaiter(1, "WaiterFemaleServer");
            }

            if (PluginConfig.SSecondWaiter.Value &&
                Game.playerCount > 1 &&
                // Game statement:
                (UpgradeScreen.checkIfUpgraded("sb") &&
                 (double)LevelController.currentLevel >= 2.0 &&
                 !__instance.isCustomLevel ||
                 __instance.isCustomLevel && ___customLevel.waiter1)
            )
            {
                ___gameController.createWaiter(2, "WaiterFemaleServer");
            }
        }
    }

    #region Old

    /*//  Transpiler first Try: Change Game.playerCount for spawning Waiters
    [HarmonyPatch(typeof(LevelController), "Start")]
    public static class LevelController_Patch_Start
    {
        // ReSharper disable once UnusedMember.Local
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> _instructions)
        {
            if (!(PluginConfig.SFirstWaiter.Value || PluginConfig.SSecondWaiter.Value))
                return _instructions;
            var codes = new List<CodeInstruction>(_instructions);
            for (var i = 0; i < codes.Count; i++)
            {
                // Line 81 in LevelController.cs // Allow waiters spawn check
                if (codes[i].opcode == OpCodes.Bgt)
                {
                    var foundBgtIndex = i;

                    // Assure the found one is the right one
                    if (codes[foundBgtIndex - 1].opcode ==
                        OpCodes.Ldc_I4_1) // PlayerCount equation checks for "1" in previous line
                    {
                        // Check if string for "Taco" is in the following lines before the next call
                        for (int j = foundBgtIndex; j < foundBgtIndex + 1000; j++)
                        {
                            if (codes[j].opcode == OpCodes.Call) // "Taco" should appear earlier than the next call
                            {
                                break;
                            }

                            if (codes[j].opcode == OpCodes.Ldstr)
                            {
                                var strTaco = codes[j].operand as string;
                                if (strTaco == "Taco")
                                {
                                    // Correct one, do:
                                    // Replace check with 1 to check with 4
                                    codes[foundBgtIndex - 1].opcode = OpCodes.Ldc_I4_4;
                                }
                            }
                        }
                    }
                }

                // Change spawn waiters check
                if (PluginConfig.SFirstWaiter.Value)
                {
                    // Replace all occurrences of "Game.playerCount == 1" with "Game.playerCount <= 4"  
                    if (codes[i].opcode == OpCodes.Ldsfld && codes[i + 1].opcode == OpCodes.Ldc_I4_1 &&
                        codes[i + 2].opcode == OpCodes.Bne_Un && codes[i + 3].opcode == OpCodes.Ldarg_0 &&
                        codes[i + 4].opcode == OpCodes.Ldfld && codes[i + 5].opcode == OpCodes.Ldc_I4_1)
                        // Finds all first appearances in each campaign if-statement
                    {
                        // Not so safe, but good enough for now:
                        codes[i + 1].opcode = OpCodes.Ldc_I4_4;
                        codes[i + 2].opcode = OpCodes.Bgt_Un; // More than 4 players -- never
                        // Game.playerCount <= 4 | branch if greater 4
                        // branch means skip (false statement)
                    }
                }

                if (PluginConfig.SSecondWaiter.Value)
                {
                    if (codes[i].opcode == OpCodes.Ldsfld && codes[i + 1].opcode == OpCodes.Ldc_I4_1 &&
                        codes[i + 2].opcode == OpCodes.Beq && codes[i + 3].opcode == OpCodes.Ldarg_0 &&
                        codes[i + 4].opcode == OpCodes.Ldfld && codes[i + 5].opcode == OpCodes.Brfalse)
                        // Finds all second appearances in each campaign if-statement
                    {
                        // Not so safe, but good enough for now:
                        codes[i + 1].opcode = OpCodes.Ldc_I4_4;
                        codes[i + 2].opcode = OpCodes.Ble_Un; // 4 or less players -- always
                        // Game.playerCount <= 4 | branch if 4 or less
                        // branch means skip remaining if parts (true statement)
                    }

                    if (codes[i].opcode == OpCodes.Ldsfld && codes[i + 1].opcode == OpCodes.Ldc_I4_1 &&
                        codes[i + 2].opcode == OpCodes.Bne_Un && codes[i + 3].opcode == OpCodes.Ldarg_0 &&
                        codes[i + 4].opcode == OpCodes.Ldfld && codes[i + 5].opcode == OpCodes.Ldc_I4_2)
                        // Finds all third appearances in each campaign if-statement
                    {
                        // Not so safe, but good enough for now:
                        codes[i + 1].opcode = OpCodes.Ldc_I4_4;
                        codes[i + 2].opcode = OpCodes.Bgt_Un; // More than 4 players -- never
                        // Game.playerCount <= 4 | branch if greater 4
                        // branch means skip (false statement)
                    }
                }
            }

            return codes.AsEnumerable();
        }
    }*/

    #endregion

    #endregion

    #region WaitersDrinks

    [HarmonyPatch(typeof(WaiterControl), "checkIfDrinkRequired")]
    public static class WaiterControlPatch
    {
        // ReSharper disable once UnusedMember.Local
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> _instructions)
        {
            // Commented to be able to enabled it midgame later
            // if (!(PluginConfig.SFirstWaiter.Value || PluginConfig.SSecondWaiter.Value))
            //     return _instructions;
            // Check against known values on startup:
            if (!PluginConfig.SServeDrinks.Value)
                return _instructions;

            var codes = new List<CodeInstruction>(_instructions);
            for (var i = 0; i < codes.Count; i++)
            {
                if (codes[i].opcode == OpCodes.Ldsfld && codes[i + 1].opcode == OpCodes.Ldc_I4_1 &&
                    codes[i + 2].opcode == OpCodes.Ble)
                {
                    codes[i + 1].opcode = OpCodes.Ldc_I4_4;
                }
            }

            return codes.AsEnumerable();
        }
    }

    #endregion

    #region TakeOrdersAutomatically

    // Take first order
    [HarmonyPatch(typeof(NPCControl), nameof(NPCControl.sitDown))]
    // ReSharper disable once InconsistentNaming
    public class NPCControl_sitDown
    {
        [HarmonyPostfix]
        // ReSharper disable once InconsistentNaming
        // ReSharper disable once UnusedMember.Local
        static void sitDownPatch(LevelController ___levelController, ref NPCControl __instance)
        {
            // Allow turn on without waiters
            // if (!(PluginConfig.SFirstWaiter.Value || PluginConfig.SSecondWaiter.Value))
            //     return;
            // Plugin settings check
            if (PluginConfig.STakeOrdersAuto.Value)
            {
                // If original method skipped automatic ordering
                if ((Game.playerCount != 1 || Math.Abs(Game.day - (-1f)) < 0.1f) &&
                    (Game.playerCount != 2 || !___levelController.isVersusMode))
                {
                    // Don't take orders automatically in the tutorial or in Versus mode
                    // (Note: The game accepts orders by default in versus with 2 players)
                    if (!(Math.Abs(Game.day - (-1.0)) < 0.1f) && (!___levelController.isVersusMode))
                    {
                        // Don't automatically accept in Challenge mode (default accept is unchanged)
                        if (!MHFunctions.IsChallengeMode(___levelController))
                        {
                            __instance.Invoke(nameof(NPCControl.orderReceived), 3f);
                        }
                    }
                }
            }
        }
    }

    // Take multiple orders
    [HarmonyPatch(typeof(NPCControl), nameof(NPCControl.stopEating))]
    // ReSharper disable once InconsistentNaming
    public class NPCControl_stopEating_TakeMultipleOrders
    {
        [HarmonyPostfix]
        // ReSharper disable once UnusedMember.Local
        // ReSharper disable InconsistentNaming
        static void StartPatch(ref NPCControl __instance, ref LevelController ___levelController)
        // ReSharper restore InconsistentNaming
        {
            // Values that always deny this feature
            if (!PluginConfig.STakeOrdersAuto.Value ||
                // !(PluginConfig.SFirstWaiter.Value || PluginConfig.SSecondWaiter.Value) || // Allow turn on without waiters
                Game.playerCount == 1) // playerCount == 1 means the game is doing this feature by itself
            {
                return;
            }

            // If skipped by game logic
            if (!(Game.playerCount == 1 && Math.Abs(Game.day - (-1.0)) > 0.1f ||
                  Game.playerCount == 2 && ___levelController.isVersusMode))
            {
                // Not in challenge mode
                if (!MHFunctions.IsChallengeMode(___levelController))
                {
                    __instance.Invoke(nameof(NPCControl.orderReceived), 3f);
                }
            }
        }
    }

    #endregion

    #region WaiterCounterSide

    [HarmonyPatch(typeof(WaiterControl), "Start")]
    // ReSharper disable once InconsistentNaming
    public class WaiterControl_Start_CounterSide
    {
        [HarmonyPostfix]
        // ReSharper disable once UnusedMember.Local
        static void StartPatch(ref WaiterControl __instance, ref GameObject[] ___counterGOs)
        {
            // Is never only 1 element, the code indicating it is removed with the transpiler
            if (PluginConfig.SWaiterCounter1.Value != 0 && __instance.waiterID == 1)
            {
                Debug.Log("Changed counter waiter to: " + PluginConfig.SWaiterCounter1.Value.ToString());
                if (!MHFunctions.SwapToFirstInArray(ref ___counterGOs, PluginConfig.SWaiterCounter1.Value))
                    Debug.Log("There is no such counter");
                __instance.setTarget(___counterGOs[0], "init 1 f");
            }

            if (PluginConfig.SWaiterCounter2.Value != 0 && __instance.waiterID == 2)
            {
                Debug.Log("Changed counter waiter to: " + PluginConfig.SWaiterCounter2.Value.ToString());
                if (!MHFunctions.SwapToFirstInArray(ref ___counterGOs, PluginConfig.SWaiterCounter2.Value))
                    Debug.Log("There is no such counter");
                __instance.setTarget(___counterGOs[0], "init 2 f");
            }
        }
    }

    #endregion

    #region RemoveWaiter2PlayerSingleCounter

    // Transpiler: Remove Waiter 2 Player limitation
    [HarmonyPatch(typeof(WaiterControl), "Start")]
    public static class WaiterControlStartPatch
    {
        // ReSharper disable once UnusedMember.Local
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> _instructions)
        {
            if (PluginConfig.SRemove2PlayerWaiterLimitation.Value)
            {
                var codes = new List<CodeInstruction>(_instructions);
                //temp
                for (int i = 0; i < codes.Count; i++)
                {
                    // Deletes all occurrences of "Game.playerCount == 2"  
                    if (codes[i].opcode == OpCodes.Ldsfld && codes[i + 1].opcode == OpCodes.Ldc_I4_2 &&
                        codes[i + 2].opcode == OpCodes.Bne_Un && codes[i + 3].opcode == OpCodes.Ldarg_0 &&
                        codes[i + 4].opcode == OpCodes.Ldc_I4_1 && codes[i + 5].opcode == OpCodes.Newarr &&
                        codes[i + 6].opcode == OpCodes.Dup)
                    {
                        codes[i].opcode = OpCodes.Nop;
                        codes.RemoveRange(i + 1, 13);
                    }
                }

                return codes.AsEnumerable();
            }

            return _instructions;
        }
    }

    #endregion

    #region CleanDishesAutomatically

    // Take multiple orders
    [HarmonyPatch(typeof(GameController), nameof(GameController.dirtyTable))]
    // ReSharper disable once InconsistentNaming
    public class NPCControl_dirtyTable_CleanDishes
    {
        [HarmonyPostfix]
        // ReSharper disable once UnusedMember.Local
        // ReSharper disable InconsistentNaming
        static void StartPatch(ref GameController __instance, ref LevelController ___levelController, int _id,
                List<GameController.Table> ___tableList)
        // ReSharper restore InconsistentNaming
        {
            // Allow turn on without waiters
            // if (!(PluginConfig.SFirstWaiter.Value || PluginConfig.SSecondWaiter.Value))
            //     return;
            // Values that always deny this feature
            if (!PluginConfig.SCleanDishesAuto.Value ||
                Game.playerCount == 1) // playerCount == 1 means the game is doing this feature by itself
            {
                return;
            }

            // If skipped by game logic
            if (!(Game.playerCount == 1 && Math.Abs(Game.day - (-1.0)) > 0.1f ||
                  Game.playerCount == 2 && ___levelController.isVersusMode))
            {
                // Not in challenge mode
                if (!MHFunctions.IsChallengeMode(___levelController))
                {
                    // Clean dishes
                    __instance.cleanTable(_id);
                    // Show dish disappearing effect
                    // ReSharper disable once ForCanBeConvertedToForeach
                    for (int index = 0; index < ___tableList.Count; ++index)
                    {
                        if (___tableList[index].id == _id)
                            __instance.createParticle("SmokePuff",
                                ___tableList[index].go.transform.Find("ItemLocation").transform.position +
                                new Vector3(0.0f, 0.2f, 0.0f), 0.0f);
                    }
                }
            }
        }
    }

    #endregion

    #region IngameModOptions

    [HarmonyPatch(typeof(MainMenu), "Start")]
    // ReSharper disable once InconsistentNaming
    public class MainMenu_Start_ModOptions
    {
        [HarmonyPostfix]
        // ReSharper disable once UnusedMember.Local
        static void StartPatch(ref MainMenu __instance, ref GameObject ___optionsScrollContentgo)
        {
            // Has to be called first
            OptionsUiElement.SetElementParent(___optionsScrollContentgo.transform);

            // ReSharper disable once StringLiteralTypo
            OptionsUiRadio.AddOriginal(___optionsScrollContentgo.transform.Find("optionsRadio qual"));
            OptionsUiToggle.AddOriginal(___optionsScrollContentgo.transform.Find("optionsToggle fs"));

            #region Configs

            /*
             * 0: Volume (Slider)
             * 1: Music (Toggle)
             * 2: Sound Effects (Toggle)
             * 3: Quality (Radio Buttons)
             * 4: Language (Button)
             * 5: Automatic Sprint (Toggle)
             * 6: Large Order Icons (Toggle)
             * 7: Joystick 1 (???)
             * 8: Joystick 2 (???)
             * 9: Full Screen (Toggle)
             * 10: Resolution (Button)
             * 11: Delete Save Files (Button)
             * 12: Delete conformation (Button)
             */

            OptionsUiToggle dishes =
                new OptionsUiToggle("Auto Clean Dishes", PluginConfig.SCleanDishesAuto.Value, ChangeCleanDishes);

            OptionsUiToggle orders = new OptionsUiToggle("Auto Take Orders",
                PluginConfig.STakeOrdersAuto.Value, ChangeTakeOrders);

            OptionsUiToggle drinks =
                new OptionsUiToggle("AI Serves Drinks (Restart)", PluginConfig.SServeDrinks.Value, ChangeServeDrinks);

            OptionsUiRadio waiter1Pos =
                new OptionsUiRadio("Waiter 1 Position", PluginConfig.SWaiterCounter1.Value, ChangeWaiter1Pos);

            OptionsUiRadio waiter2Pos =
                new OptionsUiRadio("Waiter 2 Position", PluginConfig.SWaiterCounter2.Value, ChangeWaiter2Pos);

            OptionsUiToggle waiter1Spawn =
                new OptionsUiToggle("Waiter 1 Spawn", PluginConfig.SFirstWaiter.Value, ChangeWaiter1Spawn/*, waiter1Pos*/);
            OptionsUiToggle waiter2Spawn =
                new OptionsUiToggle("Waiter 2 Spawn", PluginConfig.SSecondWaiter.Value, ChangeWaiter2Spawn/*, waiter2Pos*/);

            int i = 6;
            orders.OrderIndex = i++;
            dishes.OrderIndex = i++;
            drinks.OrderIndex = i++;
            waiter1Spawn.OrderIndex = i++;
            waiter1Pos.OrderIndex = i++;
            waiter2Spawn.OrderIndex = i++;
            waiter2Pos.OrderIndex = i++;

            #endregion
        }

        public static void ChangeServeDrinks(bool _isOn)
        {
            PluginConfig.SServeDrinks.Value = _isOn;
            MultiplayerWaiterPlugin.Instance.BindConfig(PluginConfig.SServeDrinks);
        }

        public static void ChangeCleanDishes(bool _isOn)
        {
            PluginConfig.SCleanDishesAuto.Value = _isOn;
            MultiplayerWaiterPlugin.Instance.BindConfig(PluginConfig.SCleanDishesAuto);
        }

        public static void ChangeWaiter1Pos(int _newPos)
        {
            PluginConfig.SWaiterCounter1.Value = _newPos;
            MultiplayerWaiterPlugin.Instance.BindConfig(PluginConfig.SWaiterCounter1);
        }

        public static void ChangeWaiter2Pos(int _newPos)
        {
            PluginConfig.SWaiterCounter2.Value = _newPos;
            MultiplayerWaiterPlugin.Instance.BindConfig(PluginConfig.SWaiterCounter2);
        }

        public static void ChangeTakeOrders(bool _isOn)
        {
            PluginConfig.STakeOrdersAuto.Value = _isOn;
            MultiplayerWaiterPlugin.Instance.BindConfig(PluginConfig.STakeOrdersAuto);
        }

        public static void ChangeWaiter1Spawn(bool _isOn)
        {
            PluginConfig.SFirstWaiter.Value = _isOn;
            MultiplayerWaiterPlugin.Instance.BindConfig(PluginConfig.SFirstWaiter);
        }

        public static void ChangeWaiter2Spawn(bool _isOn)
        {
            PluginConfig.SSecondWaiter.Value = _isOn;
            MultiplayerWaiterPlugin.Instance.BindConfig(PluginConfig.SSecondWaiter);
        }
    }

    #endregion

    #region BuyInMultiplayerCampaign

    [HarmonyPatch(typeof(UpgradeScreen), "generateUpgradeList")]
    public static class UpgradeScreenPatch
    {
        // ReSharper disable once UnusedMember.Local
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> _instructions, ILGenerator _generator)
        {
            var codes = new List<CodeInstruction>(_instructions);

            // Server 1
            int index = codes.FindIndex(instruction =>
                instruction.opcode == OpCodes.Ldsfld && (FieldInfo)instruction.operand ==
                Field(typeof(Game), nameof(Game.playerCount)));
            if (index != -1)
            {
                Debug.Log(index.ToString() + " " + codes[index].opcode.ToString() + " " + codes[index].operand.ToString());
                codes[index].opcode = OpCodes.Ldc_I4_1;
                Debug.Log(index.ToString() + " " + codes[index].opcode.ToString() + " " + codes[index].operand.ToString());
            }

            // Server 2
            index = codes.FindIndex(index > 0 ? index : 0, instruction =>
                 instruction.opcode == OpCodes.Ldsfld && (FieldInfo)instruction.operand ==
                 Field(typeof(Game), nameof(Game.playerCount)));
            if (index != -1)
            {
                Debug.Log(index.ToString() + " " + codes[index].opcode.ToString() + " " + codes[index].operand.ToString());
                codes[index].opcode = OpCodes.Ldc_I4_1;
                Debug.Log(index.ToString() + " " + codes[index].opcode.ToString() + " " + codes[index].operand.ToString());
            }

            return codes.AsEnumerable();
        }
    }

    #endregion
}
