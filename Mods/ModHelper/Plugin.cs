﻿using BepInEx;
using HarmonyLib;

namespace ModHelper
{
    [BepInPlugin(PluginInfo.PLUGIN_GUID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
    public class Plugin : BaseUnityPlugin
    {
        //public static ModHelper Instance { get; private set; }

        //private readonly Harmony m_harmony = new Harmony("aragami.dinerbrosplugins.ModHelper");

        private void Awake()
        {
            //if (!Instance)
            //{
            //    Instance = this;
            //}

            //m_harmony.PatchAll();
            Logger.LogInfo($"Plugin {PluginInfo.PLUGIN_GUID} is loaded!");
        }
    }
}
